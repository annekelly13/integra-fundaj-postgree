from django.contrib import admin

from .models import Contact, Board, Sector

class ContactAdmin(admin.ModelAdmin):
    def dataNasc(self, obj):
        return obj.date.strftime('%d/%m')
    list_display = ('name', 'dataNasc', 'email', 'branchLine', 'bond', 'board', 'sector', 'created', 'author')
    fieldsets = [
        ('Informações Pessoais', {'fields': ['name', 'date']}),
        ('Na Fundaj', {'fields': ['email', 'branchLine', 'bond', 'board', 'sector']}),
        ('Informações de Publicação', {'fields': ['status', 'author', 'publish']})
    ]


    class Media:
        js = ['/static/admin/js/admin-registration.js']


class BoardAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'initials', 'created', 'author')
    fieldsets = [
        ('A Diretoria', {'fields': ['name', 'initials']}),
        ('Informações de Publicação', {'fields': ['status', 'author', 'publish']})
    ]


class SectorAdmin(admin.ModelAdmin):
    
    list_display = ('name', 'initials', 'board','created', 'author')
    fieldsets = [
        ('O Setor', {'fields': ['name', 'initials', 'board']}),
        ('Informações de Publicação', {'fields': ['status', 'author', 'publish']})
    ]



# Register the admin class with the associated model
admin.site.register(Contact, ContactAdmin)
admin.site.register(Board, BoardAdmin)
admin.site.register(Sector, SectorAdmin)