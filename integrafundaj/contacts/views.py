from django.shortcuts import render
from django.views import generic
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Contact, Sector, Board
from .filters import ContactFilter

from .serializer import ContactSerializer, SectorSerializer, BoardSerializer
#from rest_framework.response import Response
#from rest_framework.views import APIView

from rest_framework import viewsets

def contacts_page_list(request):

    contact_list = Contact.objects.order_by('name')
    contact_filter = ContactFilter(request.GET, queryset=contact_list)

    page = request.GET.get('page', 1)
    paginator = Paginator(contact_filter.qs, 3)

    try:
        contacts = paginator.page(page)
    except PageNotAnInteger:
        contacts = paginator.page(1)
    except EmptyPage:
        contacts = paginator.page(paginator.num_pages)

    parameter = request.GET.copy()
    if 'page' in parameter:
        del parameter['page']

    context = {
        'filter': contact_filter,
        'contacts': contacts,
        'parameter': parameter,
    }
    return render(request, 'contacts/contact_list.html', context)


#SERIALIZAR DADOS

#classes que exibem todos os contatos, setores e diretorias serializados
class ContactListViewSet(viewsets.ModelViewSet):
    serializer_class = ContactSerializer
    queryset = Contact.objects.all()
   
class SectorListViewSet(viewsets.ModelViewSet):
    serializer_class = SectorSerializer
    queryset = Sector.objects.all()
   
class BoardListViewSet(viewsets.ModelViewSet):
    serializer_class = BoardSerializer
    queryset = Board.objects.all()



#Views que usam APIView para gear API dos dados serializados, faz o mesmo que a ViewSet, mas a ViewSet 
#é mais específica e tem uma implementacao mais ismples da paginacao
"""
class ContactListView(APIView):
    serializer_class = ContactSerializer
    def get(self, request, format=None):
        serializer = self.serializer_class(Contact.objects.all(), many=True)
        return Response(serializer.data)

#class que exibe uma contato pelo ID
class ContactView(APIView):
    def get(self,request,pk,format=None):
        user = Contact.objects.get(pk=pk)
        serializer = ContactSerializer(user)
        return Response(serializer.data)

#classe que exibe todos os setores serializados
class SectorListView(APIView):
    serializer_class = SectorSerializer
    def get(self, request, format=None):
        serializer = self.serializer_class(Sector.objects.all(), many=True)
        return Response(serializer.data)

#class que exibe uma setor pelo ID
class SectorView(APIView):
    def get(self,request,pk,format=None):
        user = Sector.objects.get(pk=pk)
        serializer = SectorSerializer(user)
        return Response(serializer.data)

class BoardListView(APIView):
    serializer_class = BoardSerializer

    def get(self, request, format=None):
        serializer = self.serializer_class(Board.objects.all(), many=True)
        return Response(serializer.data)

#class que exibe uma diretoria pelo ID
class BoardView(APIView):

    def get(self,request,pk,format=None):
        user = Board.objects.get(pk=pk)
        serializer = BoardSerializer(user)
        return Response(serializer.data)
"""