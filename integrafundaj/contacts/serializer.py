from rest_framework import serializers
from .models import Contact, Sector, Board

#Criou um serializer com daods especiais do serializer
class ContactSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contact
        depth = 1
        fields = ['id', 'name', 'date', 'email', 'branchLine', 'bond_verbose', 'registration', 'board', 'sector']

#serializer dos setores
class SectorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Sector
        depth = 1
        fields = ['id', 'name', 'initials', 'board']


#serializer  dos setores
class BoardSerializer(serializers.ModelSerializer):

    class Meta:
        model = Board
        depth = 1
        fields = ['id', 'name', 'initials']