from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Board(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    name = models.CharField(verbose_name='Nome',max_length=100)
    initials = models.CharField(verbose_name='Sigla',max_length=10)

    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    def __str__(self):
        return self.initials

    class Meta:
        verbose_name = 'Diretoria'
        verbose_name_plural = 'Diretorias'

class Sector(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    name = models.CharField(verbose_name='Setor',max_length=100)
    initials = models.CharField(verbose_name='Sigla',max_length=10)
    board = models.ForeignKey('Board',on_delete=models.SET_NULL,null=True, verbose_name='Diretoria')

    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    def __str__(self):
        return self.initials


    class Meta:
        verbose_name = 'Setor'
        verbose_name_plural = 'Setores'


class Contact(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    BOND_CHOICES = (
        ('server', 'Servidor'),
        ('outsourced', 'Terceirizado'),
        ('trainee', 'Estagiário'),
    )

    name = models.CharField(verbose_name='Nome', max_length=100)
    date = models.DateField(verbose_name='Data de Nascimento',
                            help_text='Preencha no formato: dia/mês/ano. Ex: 24/07/1913')
    email = models.CharField(verbose_name='Email', max_length=100)
    branchLine = models.DecimalField(verbose_name='Ramal', max_digits=4, decimal_places=0)
    bond = models.CharField(verbose_name='Vínculo',
                            max_length=10,
                            choices=BOND_CHOICES)
    registration = models.CharField(verbose_name='Matrícula', max_length=20)
    board = models.ForeignKey('Board', on_delete=models.SET_NULL, null=True, verbose_name='Diretoria')
    sector = models.ForeignKey('Sector', on_delete=models.SET_NULL, null=True, verbose_name='Setor')

    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    def bond_verbose(self):
        return dict(Contact.BOND_CHOICES)[self.bond]

    class Meta:
        verbose_name = 'Contato'
        verbose_name_plural = 'Contatos'

    def __str__(self):
        return self.name