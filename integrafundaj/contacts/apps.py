from django.apps import AppConfig


class ContactsConfig(AppConfig):
    name = 'integrafundaj.contacts'
    verbose_name = 'Controle de Contatos'