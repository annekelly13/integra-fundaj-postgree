from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator

# Create your models here.
class Year(models.Model):
    year = models.CharField(verbose_name='Ano', max_length=10)

    class Meta:
        verbose_name = 'Ano'
        verbose_name_plural = 'Anos'

    def __str__(self):
        return self.year

class Writer(models.Model):
    name = models.CharField(verbose_name='Nome', max_length=20)
    last_name = models.CharField(verbose_name='Sobrenome', max_length=50, default='')

    def __str__(self):
        return '%s (%s)' % (self.last_name,self.name)

    class Meta:
        verbose_name = 'Escritor'
        verbose_name_plural = 'Escritores'


class Book(models.Model):

  # STATUS_CHOICES = (
    #    ('launch', 'Lançamento'),
   #     ('collection', 'Acervo'),
   # )

    LANGUAGE_CHOICES = (
        ('PT', 'Português'),
        ('EN', 'Inglês'),
        ('ES', 'Espanhol'),
        ('--', 'Outro'),
    )

    book = models.CharField(verbose_name='Título', max_length=200)
    book_cover = models.ImageField(verbose_name='Capa do Livro', upload_to='publishers/book_covers')
    writer = models.ManyToManyField('Writer', verbose_name='Escritor(es)')

    synopsis = models.TextField(verbose_name='Sinopse:', max_length=500, default='')
    language = models.CharField(verbose_name='Idioma',max_length=10,choices=LANGUAGE_CHOICES, default='PT')
    
    year = models.ForeignKey('Year', on_delete=models.SET_NULL, null=True, verbose_name='Ano')
    pages = models.PositiveIntegerField(verbose_name='Número de Páginas', default=0)
    
    weight = models.DecimalField(verbose_name='Peso', max_digits=6, decimal_places=2,default=0.00, validators=[MinValueValidator(0)] )
    width = models.DecimalField(verbose_name='Largura(cm)', max_digits=6, decimal_places=2,default=0.00, validators=[MinValueValidator(0)] )
    height = models.DecimalField(verbose_name='Altura(cm)', max_digits=6, decimal_places=2,default=0.00, validators=[MinValueValidator(0)] )

    price = models.DecimalField(verbose_name='Preço(R$)', max_digits=8, decimal_places=2,default=0.00, validators=[MinValueValidator(0)] )
    isbn = models.CharField(verbose_name='ISBN:',max_length=13, help_text='13 Caracteres')
    
    """status = models.CharField(verbose_name='Status',
                             max_length=10,
                              choices=STATUS_CHOICES,
                              default='collection')"""

    edition = models.CharField(verbose_name='Edição',max_length=60, default= ' ')
    genre = models.CharField(verbose_name='Gênero',max_length=60, default= ' ')
    
    
    
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)


    def __str__(self):
        return self.book

    # Cria uma string a partir dos 3 primeiros nomes de autores (se existe)
    def display_writer(self):
        """
        Creates a string for the Genre. This is required to display genre in Admin.
        """
        return ', '.join([ writer.name for writer in self.writer.all()[:3] ])
    display_writer.short_description = 'Writer'


    class Meta:
        verbose_name = 'Livro'
        verbose_name_plural = 'Livros'



class Information(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    STATE_CHOICES = (
        ('AC', 'AC'),
        ('AL', 'AL'),
        ('AP', 'AP'),
        ('AM', 'AM'),
        ('BH', 'BH'),
        ('CE', 'CE'),
        ('DF', 'DF'),
        ('ES', 'ES'),
        ('GO', 'GO'),
        ('MA', 'MA'),
        ('MT', 'MT'),
        ('MS', 'MS'),
        ('MG', 'MG'),
        ('PA', 'PA'),
        ('PB', 'PB'),
        ('PR', 'PR'),
        ('PE', 'PE'),
        ('PI', 'PI'),
        ('RJ', 'RJ'),
        ('RN', 'RN'),
        ('RS', 'RS'),
        ('RO', 'RO'),
        ('RR', 'RR'),
        ('SC', 'SC'),
        ('SP', 'SP'),
        ('SE', 'SE'),
        ('TO', 'TO'),
    )
    
    local = models.CharField(verbose_name='Local', max_length=100, default='---')
    adress = models.CharField(verbose_name='Rua/Av', max_length=300, null=True, blank=True)
    number = models.CharField(verbose_name='Número', max_length=6, null=True, blank=True)
    cep = models.CharField(verbose_name='Cep', max_length=15, null=True, blank=True)
    complement = models.CharField(verbose_name='Complemento', max_length=300, null=True, blank=True)
    neighborhood = models.CharField(verbose_name='Bairro', max_length=200)
    city = models.CharField(verbose_name='Cidade', max_length=100)
    state = models.CharField(verbose_name='Estado', max_length=20, choices=STATE_CHOICES, default='PE')
    
    phone_number = models.CharField(verbose_name='Telefone', max_length=12)
    email = models.CharField(verbose_name='Email', max_length=60)
    #local = models.ForeignKey('Local', on_delete=models.SET_NULL, null=True)
    
    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True, related_name='autor')
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    def __str__(self):
        return '%s - %s - %s' % (self.local, self.phone_number, self.email)

    class Meta:
        verbose_name = 'Informação'
        verbose_name_plural = 'Informações'