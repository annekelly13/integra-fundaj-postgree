from django.shortcuts import render

from .models import Book, Writer, Year, Information

from .serializer import BookSerializer, WriterSerializer, YearSerializer, InformationSerializer
from rest_framework import viewsets

#classe que exibe todos os livros serializados
class BookListViewSet(viewsets.ModelViewSet):
    serializer_class = BookSerializer
    queryset = Book.objects.all()


#classe que exibe todos os anos serializados
class YearListViewSet(viewsets.ModelViewSet):
    serializer_class = YearSerializer
    queryset = Year.objects.all()


#classe que exibe todos os escritores serializados
class WriterListViewSet(viewsets.ModelViewSet):
    serializer_class = WriterSerializer
    queryset = Writer.objects.all()


#classe que exibe todos as informacoes serializadas
class InformationPublishListViewSet(viewsets.ModelViewSet):
    serializer_class = InformationSerializer
    queryset = Information.objects.all()
