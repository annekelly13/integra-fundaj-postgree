from django.apps import AppConfig


class PublishersConfig(AppConfig):
    name = 'integrafundaj.publishers'
    verbose_name = 'Controle da Editora' 