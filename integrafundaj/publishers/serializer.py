from rest_framework import serializers
from .models import Book, Writer, Year, Information

#Criou um serializer com daods especiais do serializer
class BookSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        depth = 1
        fields = ['id', 'book', 'book_cover', 'writer', 'synopsis', 'language', 'year', 'pages', 'weight', 'width', 'height', 'price', 'isbn', 'edition', 'genre']

#serializer dos setores
class WriterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Writer
        depth = 1
        fields = ['id', 'name', 'last_name']


#serializer  dos setores
class YearSerializer(serializers.ModelSerializer):

    class Meta:
        model = Year
        depth = 1
        fields = ['id', 'year']


#Criou um serializer com daods especiais do serializer
class InformationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Information
        depth = 1
        fields = ['id', 'local', 'adress', 'number', 'cep', 'complement', 'neighborhood', 'city', 'state', 'phone_number', 'email']