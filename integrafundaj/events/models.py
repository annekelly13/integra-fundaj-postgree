from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.
class Event(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    CATEGORY_CHOICES = (
        ('general', 'Geral'),
        ('course', 'Curso'),
        ('seminary', 'Seminário'),
        ('meeting', 'Encontro'),
    )

    STATE_CHOICES = (
        ('AC', 'AC'),
        ('AL', 'AL'),
        ('AP', 'AP'),
        ('AM', 'AM'),
        ('BH', 'BH'),
        ('CE', 'CE'),
        ('DF', 'DF'),
        ('ES', 'ES'),
        ('GO', 'GO'),
        ('MA', 'MA'),
        ('MT', 'MT'),
        ('MS', 'MS'),
        ('MG', 'MG'),
        ('PA', 'PA'),
        ('PB', 'PB'),
        ('PR', 'PR'),
        ('PE', 'PE'),
        ('PI', 'PI'),
        ('RJ', 'RJ'),
        ('RN', 'RN'),
        ('RS', 'RS'),
        ('RO', 'RO'),
        ('RR', 'RR'),
        ('SC', 'SC'),
        ('SP', 'SP'),
        ('SE', 'SE'),
        ('TO', 'TO'),
    )

    title = models.CharField(verbose_name='Título', max_length=200)
    category = models.CharField(verbose_name='Categoria', max_length=30, choices=CATEGORY_CHOICES, default='general')
    banner = models.ImageField(verbose_name='Banner', upload_to='events/banners')
    description = models.TextField(verbose_name='Descrição', max_length=200)

    site_event = models.URLField(verbose_name='Site', null=True, blank=True)
    subscription = models.CharField(verbose_name='Inscrição', max_length=40, null=True, blank=True)
    email = models.EmailField(verbose_name='E-mail', null=True, blank=True)
    phone_number = models.CharField(verbose_name='Telefone', max_length=12, null=True, blank=True)

    date_start = models.DateTimeField(verbose_name='Data de início', default=timezone.now)
    date_end = models.DateTimeField(verbose_name='Data de término', default=timezone.now)

    event_place = models.CharField(verbose_name='Local do Evento', max_length=100, default='---')
    adress = models.CharField(verbose_name='Rua/Av', max_length=300, null=True, blank=True)
    number = models.CharField(verbose_name='Número', max_length=6, null=True, blank=True)
    cep = models.CharField(verbose_name='Cep', max_length=15, null=True, blank=True)
    complement = models.CharField(verbose_name='Complemento', max_length=300, null=True, blank=True)
    neighborhood = models.CharField(verbose_name='Bairro', max_length=200)
    city = models.CharField(verbose_name='Cidade', max_length=100)
    state = models.CharField(verbose_name='Estado', max_length=20, choices=STATE_CHOICES, default='PE')

    favorite = models.BooleanField(verbose_name='Favorito', default=False)

    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    def __str__(self):
        return self.title

    #Funcao que exibe o tipo do evento em portugues no Json e na View
    def category_verbose(self):
        return dict(Event.CATEGORY_CHOICES)[self.category]

    class Meta:
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'
        ordering = ('-date_start',)