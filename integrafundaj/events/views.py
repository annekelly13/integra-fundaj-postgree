from django.shortcuts import render

from .models import Event

from .serializer import EventSerializer
#from rest_framework.response import Response
#from rest_framework.views import APIView

from rest_framework import viewsets

#classe que exibe todos os eventos serializados
class EventListViewSet(viewsets.ModelViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.all()

#Views que usam APIView para gear API dos dados serializados, faz o mesmo que a ViewSet, mas a ViewSet 
#é mais específica e tem uma implementacao mais ismples da paginacao
"""class EventListView(APIView):
    serializer_class = EventSerializer
    def get(self, request, format=None):
        serializer = self.serializer_class(Event.objects.all(), many=True)
        return Response(serializer.data)

#class que exibe uma evento pelo ID
class EventView(APIView):

    def get(self,request,pk,format=None):
        user = Event.objects.get(pk=pk)
        serializer = EventSerializer(user)
        return Response(serializer.data)"""