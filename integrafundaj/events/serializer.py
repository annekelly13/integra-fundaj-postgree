from rest_framework import serializers
from .models import Event

#Criou um serializer com daods especiais do serializer
class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        depth = 1
        fields = ['id', 'title', 'category_verbose', 'banner', 'description', 'site_event', 'subscription',
                  'email', 'phone_number', 'date_start', 'date_end', 'event_place', 'adress', 'number',
                  'cep', 'complement', 'neighborhood', 'city', 'state', 'favorite']
