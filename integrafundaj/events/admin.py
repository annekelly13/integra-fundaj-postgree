from django.contrib import admin
from django import forms

from django.core.files.uploadedfile import UploadedFile
from django.db.models.fields.files import ImageFieldFile

from PIL import Image

# Register your models here.
from .models import Event

# Validação customizada do campo de banner
class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = '__all__'
    
    def clean_banner(self):
        image = self.cleaned_data.get('banner', False)
        if image:
            img = Image.open(image)
            width, height = img.size

            # Valores padrão do banner 879 x 392 pixels
            max_width = 879
            max_height = 392

            # Validando as dimensões do banner
            if width > max_width or width < max_width or height > max_height or height < max_height: 
                raise forms.ValidationError('Imagem está com dimensão incorreta: %s x %s pixels. Insira uma imagem com %s x %s pixels.'
                                            %(width, height, max_width, max_height))
            # Validando o tamanho do arquivo - Máximo 3MB
            if len(image) > (3 * 1024 * 1024):
                raise forms.ValidationError('Arquivo de imagem muito grande (máximo 3MB).')
            
            #Validação da extensão da imagem
            name_img, ext = image.name.split('.')
            if not (ext.lower() in ['png', 'jpg', 'jpeg']):
               raise forms.ValidationError('Por favor, use imagem em formato JPG, JPEG ou PNG.')
        else:
            raise forms.ValidationError('Não foi possível ler a imagem carregada.')
        return image


# Customização da exibição dos dados 
class EventAdmin(admin.ModelAdmin):
    form = EventForm
    list_display = ('title', 'category', 'status', 'date_start', 'date_end','created', 'author')
    fieldsets = [
        ('Informações do Evento', {'fields': ['title', 'category', 'banner', 'description', 'site_event', 'subscription', 'email', 'phone_number']}),
        ('Quando?', {'fields': [('date_start', 'date_end')]}),
        ('Onde?', {'fields': ['event_place', ('adress', 'number'), 'cep', 'complement', 'neighborhood', ('city', 'state'), ]}),
        ('Informações de Publicação', {'fields': ['status', 'author', 'publish']})
    ]

# Register the admin class with the associated model
admin.site.register(Event, EventAdmin)