from django.apps import AppConfig


class EventsConfig(AppConfig):
    name = 'integrafundaj.events'
    verbose_name = 'Controle de Eventos'
