from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Artist(models.Model):
    name = models.CharField(verbose_name='Nome', max_length=20)
    last_name = models.CharField(verbose_name='Sobrenome', max_length=50, default='')

    def __str__(self):
        return '%s (%s)' % (self.last_name,self.name)

    class Meta:
        verbose_name = 'Artista'
        verbose_name_plural = 'Artistas'

# Create your models here.
class Piece(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    CATEGORY_CHOICES = (
        ('general', 'Geral'),
        ('picture', 'Pintura'),
        ('sculpture', 'Escultura'),
        ('tapestry', 'Tapeçaria'),
        ('clothes', 'Vestimentas'),
        ('utensils', 'Utensílios'),
    )

    name = models.CharField(verbose_name="Nome", max_length=200)
    image = models.ImageField(verbose_name='Imagem', upload_to='museum/collection')
    artist = models.ManyToManyField('Artist', verbose_name='Artista(s)')
    
    category = models.CharField(verbose_name='Categoria', max_length=30,choices=CATEGORY_CHOICES,default='general')
    description = models.TextField(verbose_name='Descrição',max_length=200)
    material = models.CharField(verbose_name="Material", max_length=300, default='---')
    
    date = models.DateTimeField(verbose_name='Data', default=timezone.now) 
    place_of_origin = models.CharField(verbose_name='Local', max_length=100, default='---')

    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    # Cria uma string a partir dos 3 primeiros nomes de autores (se existe)
    def display_artist(self):
        return ', '.join([ artist.name for artist in self.artist.all()[:3] ])
    display_artist.short_description = 'Artista'

    def __str__(self):
        return '%s (%s)' % (self.name, self.category)

    class Meta:
        verbose_name = 'Peça'
        verbose_name_plural = 'Acervo'


class Exhibition(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    SITUATION_CHOICES = (
        ('yes', 'Sim'),
        ('no', 'Não'),
    )

    title = models.CharField(verbose_name="Nome", max_length=200)
    banner = models.ImageField(verbose_name='Banner', upload_to='museum/exhibitions')
    #artist = models.CharField(verbose_name="Artista:", max_length=60, default='')
    artist = models.ManyToManyField('Artist', verbose_name='Artista(s)')

    date_start = models.DateTimeField(verbose_name='Data de início', default=timezone.now)
    date_end = models.DateTimeField(verbose_name='Data de término', default=timezone.now)
    
    place_of_exhibition = models.CharField(verbose_name='Local da Exposição', max_length=100, default='---')
    description = models.TextField(verbose_name='Descrição',max_length=200)
    situation = models.CharField( verbose_name='Em exibição', max_length=5, choices=SITUATION_CHOICES, default='yes')

    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    # Cria uma string a partir dos 3 primeiros nomes de autores (se existe)
    def display_artist(self):
        return ', '.join([ artist.name for artist in self.artist.all()[:3] ])
    display_artist.short_description = 'Artista'

    def __str__(self):
        return '%s' % (self.title)

    class Meta:
        verbose_name = 'Exposição'
        verbose_name_plural = 'Exposições Temporárias'



class Information(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    STATE_CHOICES = (
        ('AC', 'AC'),
        ('AL', 'AL'),
        ('AP', 'AP'),
        ('AM', 'AM'),
        ('BH', 'BH'),
        ('CE', 'CE'),
        ('DF', 'DF'),
        ('ES', 'ES'),
        ('GO', 'GO'),
        ('MA', 'MA'),
        ('MT', 'MT'),
        ('MS', 'MS'),
        ('MG', 'MG'),
        ('PA', 'PA'),
        ('PB', 'PB'),
        ('PR', 'PR'),
        ('PE', 'PE'),
        ('PI', 'PI'),
        ('RJ', 'RJ'),
        ('RN', 'RN'),
        ('RS', 'RS'),
        ('RO', 'RO'),
        ('RR', 'RR'),
        ('SC', 'SC'),
        ('SP', 'SP'),
        ('SE', 'SE'),
        ('TO', 'TO'),
    )
    
    local = models.CharField(verbose_name='Local', max_length=100, default='---')
    adress = models.CharField(verbose_name='Rua/Av', max_length=300, null=True, blank=True)
    number = models.CharField(verbose_name='Número', max_length=6, null=True, blank=True)
    cep = models.CharField(verbose_name='Cep', max_length=15, null=True, blank=True)
    complement = models.CharField(verbose_name='Complemento', max_length=300, null=True, blank=True)
    neighborhood = models.CharField(verbose_name='Bairro', max_length=200)
    city = models.CharField(verbose_name='Cidade', max_length=100)
    state = models.CharField(verbose_name='Estado', max_length=20, choices=STATE_CHOICES, default='PE')
    
    phone_number = models.CharField(verbose_name='Telefone', max_length=12)
    email = models.CharField(verbose_name='Email', max_length=60)
    #local = models.ForeignKey('Local', on_delete=models.SET_NULL, null=True)

    business_hours = models.CharField(verbose_name='Horário de Funcionamento', max_length=100)
    half_entrance = models.TextField(verbose_name='Meia Entrada',max_length=100)
    tickets = models.TextField( verbose_name='Ingressos',max_length=100)

    
    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    def __str__(self):
        return '%s - %s - %s' % (self.local, self.phone_number, self.email)

    class Meta:
        verbose_name = 'Informação'
        verbose_name_plural = 'Informações'