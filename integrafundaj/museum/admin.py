from django.contrib import admin
from django import forms

from django.core.files.uploadedfile import UploadedFile
from django.db.models.fields.files import ImageFieldFile

from PIL import Image

# Register your models here.
from .models import Artist, Piece, Exhibition, Information

admin.site.register(Artist)


# Validação customizada do campo de image
class PieceForm(forms.ModelForm):
    class Meta:
        model = Piece
        fields = '__all__'
    
    def clean_banner(self):
        image = self.cleaned_data.get('image', False)
        if image:
            img = Image.open(image)
            width, height = img.size

            # Valores padrão do banner 879 x 392 pixels
            max_width = 879
            max_height = 392

            # Validando as dimensões do banner
            if width > max_width or width < max_width or height > max_height or height < max_height: 
                raise forms.ValidationError('Imagem está com dimensão incorreta: %s x %s pixels. Insira uma imagem com %s x %s pixels.'
                                            %(width, height, max_width, max_height))
            # Validando o tamanho do arquivo - Máximo 3MB
            if len(image) > (3 * 1024 * 1024):
                raise forms.ValidationError('Arquivo de imagem muito grande (máximo 3MB).')
            
            #Validação da extensão da imagem
            name_img, ext = image.name.split('.')
            if not (ext.lower() in ['png', 'jpg', 'jpeg']):
               raise forms.ValidationError('Por favor, use imagem em formato JPG, JPEG ou PNG.')
        else:
            raise forms.ValidationError('Não foi possível ler a imagem carregada.')
        return image


# Validação customizada do campo de banner
class ExhibitionForm(forms.ModelForm):
    class Meta:
        model = Exhibition
        fields = '__all__'
    
    def clean_banner(self):
        image = self.cleaned_data.get('banner', False)
        if image:
            img = Image.open(image)
            width, height = img.size

            # Valores padrão do banner 879 x 392 pixels
            max_width = 879
            max_height = 392

            # Validando as dimensões do banner
            if width > max_width or width < max_width or height > max_height or height < max_height: 
                raise forms.ValidationError('Imagem está com dimensão incorreta: %s x %s pixels. Insira uma imagem com %s x %s pixels.'
                                            %(width, height, max_width, max_height))
            # Validando o tamanho do arquivo - Máximo 3MB
            if len(image) > (3 * 1024 * 1024):
                raise forms.ValidationError('Arquivo de imagem muito grande (máximo 3MB).')
            
            #Validação da extensão da imagem
            name_img, ext = image.name.split('.')
            if not (ext.lower() in ['png', 'jpg', 'jpeg']):
               raise forms.ValidationError('Por favor, use imagem em formato JPG, JPEG ou PNG.')
        else:
            raise forms.ValidationError('Não foi possível ler a imagem carregada.')
        return image


# Customização da exibição dos dados 
class PieceAdmin(admin.ModelAdmin):
    form = PieceForm
    list_display = ('name', 'display_artist', 'category', 'created', 'author')
    fieldsets = [
        ('Informações da Peça', {'fields': ['name', 'image', 'artist' , 'category', 'material', 'description', ]}),
        ('Origem da Peça', {'fields': [('date', 'place_of_origin')]}),
        ('Informações de Publicação', {'fields': ['status', 'author', 'publish']})
    ]



# Customização da exibição dos dados 
class ExhibitionAdmin(admin.ModelAdmin):
    form = ExhibitionForm
    list_display = ('title', 'display_artist', 'date_start', 'date_end', 'place_of_exhibition', 'created', 'author')
    fieldsets = [
        ('Informações da Exposição', {'fields': ['title', 'banner', 'artist' , 'description', 'situation']}),
        ('Quando?', {'fields': [('date_start', 'date_end') ]}),
        ('Onde?', {'fields': ['place_of_exhibition']}),
        ('Informações de Publicação', {'fields': ['status', 'author', 'publish']})
    ]


# Customização da exibição dos dados 
class InformationAdmin(admin.ModelAdmin):
    list_display = ('local', 'phone_number', 'email', 'created', 'author')
    fieldsets = [
        ('Onde?', {'fields': [ 'local', ('adress', 'number'), 'cep', 'complement', 'neighborhood', ('city', 'state') ]}),
        ('Contato', {'fields': ['phone_number', 'email']}),
        ('Horário de Funcionamento', {'fields': ['business_hours']}),
        ('Ingressos', {'fields': ['tickets']}),
        ('Meia Entrada', {'fields': ['half_entrance']}),
        ('Informações de Publicação', {'fields': ['status', 'author', 'publish']})
    ]



# Register the admin class with the associated model
admin.site.register(Piece, PieceAdmin)
admin.site.register(Exhibition, ExhibitionAdmin)
admin.site.register(Information, InformationAdmin)
