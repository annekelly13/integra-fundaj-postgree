from django.shortcuts import render

from .models import Artist, Piece, Exhibition, Information
from .serializer import ArtistSerializer, PieceSerializer, ExhibitionSerializer, InformationSerializer
from rest_framework import viewsets

#classe que exibe todos os artistas serializados
class ArtistListViewSet(viewsets.ModelViewSet):
    serializer_class = ArtistSerializer
    queryset = Artist.objects.all()

#classe que exibe todas as pecas serializadas
class PieceListViewSet(viewsets.ModelViewSet):
    serializer_class = PieceSerializer
    queryset = Piece.objects.all()

#classe que exibe todos as exposicoes serializados
class ExhibitionListViewSet(viewsets.ModelViewSet):
    serializer_class = ExhibitionSerializer
    queryset = Exhibition.objects.all()

#classe que exibe todos as informacoes serializadas
class InformationListViewSet(viewsets.ModelViewSet):
    serializer_class = InformationSerializer
    queryset = Information.objects.all()