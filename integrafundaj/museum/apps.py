from django.apps import AppConfig


class MuseumConfig(AppConfig):
    name = 'integrafundaj.museum'
    verbose_name = 'Controle do Museu'