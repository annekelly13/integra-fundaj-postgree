from rest_framework import serializers
from .models import Artist, Piece, Exhibition, Information

#Criou um serializer com daods especiais do serializer
class ArtistSerializer(serializers.ModelSerializer):

    class Meta:
        model = Artist
        depth = 1
        fields = ['id', 'name', 'last_name']


#Criou um serializer com daods especiais do serializer
class PieceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Piece
        depth = 1
        fields = ['id', 'name', 'image', 'artist', 'category', 'description', 'material', 'date', 'place_of_origin']


#Criou um serializer com daods especiais do serializer
class ExhibitionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Exhibition
        depth = 1
        fields = ['id', 'title', 'banner', 'artist', 'date_start', 'date_end', 'place_of_exhibition', 'description', 'situation']


#Criou um serializer com daods especiais do serializer
class InformationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Information
        depth = 1
        fields = ['id', 'local', 'adress', 'number', 'cep', 'complement', 'neighborhood', 'city', 'state', 'phone_number', 'email', 'business_hours', 'half_entrance', 'tickets']
