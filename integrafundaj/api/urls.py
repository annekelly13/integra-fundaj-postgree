from django.conf.urls import include, url
from integrafundaj.contacts.views import ContactListViewSet, SectorListViewSet, BoardListViewSet
from integrafundaj.events.views import  EventListViewSet
from integrafundaj.biddings.views import BiddingListViewSet, YearListViewSet
from integrafundaj.publishers.views import BookListViewSet, WriterListViewSet, YearListViewSet, InformationPublishListViewSet
from integrafundaj.museum.views import ArtistListViewSet, PieceListViewSet, ExhibitionListViewSet, InformationListViewSet

from rest_framework import routers

#Gerando rotas com os urls arelados aos links da API, usando ViewSet
router = routers.DefaultRouter()
router.register(r'eventos', EventListViewSet)
router.register(r'contatos', ContactListViewSet)
router.register(r'contatos-setores', SectorListViewSet)
router.register(r'contatos-diretorias', BoardListViewSet)
router.register(r'licitacoes', BiddingListViewSet)
router.register(r'licitacoes-anos', YearListViewSet)
router.register(r'editora', BookListViewSet)
router.register(r'editora-escritores', WriterListViewSet)
router.register(r'editora-anos', YearListViewSet)
router.register(r'editora-informacoes', InformationPublishListViewSet)
router.register(r'museu-acervo', PieceListViewSet)
router.register(r'museu-artistas', ArtistListViewSet)
router.register(r'museu-exposicoes', ExhibitionListViewSet)
router.register(r'museu-informacoes', InformationListViewSet)

#Forma de configurar urls da API, quando as vies usam APIView e nao ViewSet
"""helper_patterns = [
    url(r'^contatos/$', ContactListView.as_view(), name='get_contatos'),
    url(r'^contatos/(?P<pk>[0-9]+)/$', ContactView.as_view(), name='contato'),
    url(r'^contatos/setores/$', SectorListView.as_view(), name='get_setores'),
    url(r'^contatos/setores/(?P<pk>[0-9]+)/$', SectorView.as_view(), name='setor'),
    url(r'^contatos/diretorias/$', BoardListView.as_view(), name='get_diretorias'),
    url(r'^contatos/diretorias/(?P<pk>[0-9]+)/$', BoardView.as_view(), name='diretoria'),
    url(r'^eventos/$', EventListView.as_view(), name='get_eventos'),
    url(r'^eventos/(?P<pk>[0-9]+)/$', EventView.as_view(), name='evento'),
    url(r'^licitacoes/$', BiddingListView.as_view(), name='get_licitacoes'),
    url(r'^licitacoes/(?P<pk>[0-9]+)/$', BiddingView.as_view(), name='licitacao'),
    url(r'^licitacoes/anos/$', YearListView.as_view(), name='get_anos'),
    url(r'^licitacoes/anos/(?P<pk>[0-9]+)/$', YearView.as_view(), name='ano'),
]
"""
urlpatterns = router.urls
#urlpatterns = helper_patterns