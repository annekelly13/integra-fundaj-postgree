from django.apps import AppConfig


class BiddingsConfig(AppConfig):
    name = 'integrafundaj.biddings'
    verbose_name = 'Controle das Licitações'