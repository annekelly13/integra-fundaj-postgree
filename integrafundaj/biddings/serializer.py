from rest_framework import serializers
from .models import Year, Bidding

#Criou um serializer com daods especiais do serializer
class BiddingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bidding
        depth = 1
        fields = ['id','year', 'notice','status_verbose','date', 'process', 'modality_verbose', 'objectt', 'price','winner', 'document']

class YearSerializer(serializers.ModelSerializer):

    class Meta:
        model = Year
        depth = 1
        fields = ['id','year']
