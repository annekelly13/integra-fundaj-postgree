from django.db import models
from django.core.validators import MinValueValidator
from django.utils import timezone
from filer.fields.file import FilerFileField
from django.contrib.auth.models import User

class Year(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    year = models.CharField(verbose_name='Ano', max_length=10)

    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    class Meta:
        verbose_name = 'Ano'
        verbose_name_plural = 'Anos'

    def __str__(self):
        return self.year


# Create your models here.
class Bidding(models.Model):

    STATUS_CHOICES = (
        ('draft', 'Rascunho'),
        ('published', 'Publicado'),
        ('closed', 'Encerrado'),
    )

    MODALITY_CHOICES = (
        ('electronicTrading', 'Pregão eletrônico'),
        ('electronicTradingPrice', 'Pregão eletrônico por registro de preço'),
        ('competition', 'Concorrência'),
        ('faceTrading', 'Pregão presencial'),
        ('invitation', 'Convite'),
        ('sale', 'Leilão'),
        ('pricingTaking', 'Tomada de preços'),
    )

    SITUATION_CHOICES = (
        ('OpeningSession', 'Sessão de Abertura'),
        ('legalRecourse', 'Recurso'),
        ('award', 'Adjudicação'),
        ('homologation', 'Homologação'),
        ('nullified', 'Anulado'),
        ('revoked', 'Revogado'),
        ('failed', 'Fracassado'),
        ('waste', 'Deserto'),
        ('acceptance', 'Aceitação'),

    )

    year = models.ForeignKey('Year', on_delete=models.SET_NULL, null=True, verbose_name='Ano')
    notice = models.PositiveIntegerField(verbose_name='Número do Edital', default=0)
    situation = models.CharField(verbose_name='Fase', max_length=50, choices=SITUATION_CHOICES)

    date = models.DateField(verbose_name='Data de Abertura',
                            help_text='Preencha no formato: dia/mês/ano. Ex: 24/07/1913', default=timezone.now)
    process = models.CharField(verbose_name='Número do Processo', max_length=50, default='')
    modality = models.CharField(verbose_name='Modalidade',
                                max_length=30,
                                choices=MODALITY_CHOICES)
    object_name = models.CharField(verbose_name='Objeto', max_length=50, default='')
    price = models.DecimalField(verbose_name='Valor do Contrato(R$):', max_digits=20, decimal_places=2, default=0.00,
                                validators=[MinValueValidator(0)])
    winner = models.CharField(verbose_name='Vencedor', max_length=100, default='')

    document = FilerFileField(null=True, blank=True, related_name='document_edital')

    uploaded_at = models.DateTimeField(auto_now_add=True)

    status = models.CharField(verbose_name='Status', max_length=10, choices=STATUS_CHOICES, default='draft')
    author = models.ForeignKey(User, verbose_name='Autor(a)', null=True)
    publish = models.DateTimeField(verbose_name='Publicar em', default=timezone.now)
    created = models.DateTimeField(verbose_name='Criado em', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='Atualizado em', auto_now=True)

    def modality_verbose(self):
        return dict(Bidding.MODALITY_CHOICES)[self.modality]

    def situation_verbose(self):
        return dict(Bidding.SITUATION_CHOICES)[self.situation]

    class Meta:
        verbose_name = 'Licitação'
        verbose_name_plural = 'Licitações'

    def __str__(self):
        return self.modality