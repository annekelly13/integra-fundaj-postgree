from django.shortcuts import render
from django.views import generic
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Bidding, Year
from .filters import BiddingFilter

from .serializer import BiddingSerializer, YearSerializer
from rest_framework import viewsets
#from rest_framework.response import Response
#from rest_framework.views import APIView

def biddings_page_list(request):

    bidding_list = Bidding.objects.order_by('process')
    bidding_filter = BiddingFilter(request.GET, queryset=bidding_list)

    page = request.GET.get('page', 1)
    paginator = Paginator(bidding_filter.qs, 3)

    try:
        biddings = paginator.page(page)
    except PageNotAnInteger:
        biddings = paginator.page(1)
    except EmptyPage:
        biddings = paginator.page(paginator.num_pages)

    parameter = request.GET.copy()
    if 'page' in parameter:
        del parameter['page']

    context = {
        'filter': bidding_filter,
        'biddings': biddings,
        'parameter': parameter,
    }
    return render(request, 'biddings/bidding_list.html', context)


#Serializar os dados

class BiddingListViewSet(viewsets.ModelViewSet):
    serializer_class = BiddingSerializer
    queryset = Bidding.objects.all()

class YearListViewSet(viewsets.ModelViewSet):
    serializer_class = YearSerializer
    queryset = Year.objects.all()

#Views que usam APIView para gear API dos dados serializados, faz o mesmo que a ViewSet, mas a ViewSet 
#é mais específica e tem uma implementacao mais ismples da paginacao
"""
class BiddingListView(APIView):
    serializer_class = BiddingSerializer

    def get(self, request, format=None):
        serializer = self.serializer_class(Bidding.objects.all(), many=True)
        return Response(serializer.data)

#class que exibe uma licitacao pelo ID
class BiddingView(APIView):

    def get(self,request,pk,format=None):
        user = Bidding.objects.get(pk=pk)
        serializer = BiddingSerializer(user)
        return Response(serializer.data)


class YearListView(APIView):
    serializer_class = YearSerializer

    def get(self, request, format=None):
        serializer = self.serializer_class(Year.objects.all(), many=True)
        return Response(serializer.data)

#class que exibe uma ano pelo ID
class YearView(APIView):

    def get(self,request,pk,format=None):
        user = Year.objects.get(pk=pk)
        serializer = YearSerializer(user)
        return Response(serializer.data)"""