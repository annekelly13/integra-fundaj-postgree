from django.contrib import admin
from .models import Bidding, Year


class BiddingAdmin(admin.ModelAdmin):
    def dataDeAbertura(self, obj):
        return obj.date.strftime('%d/%m/%y')

    list_display = ('year', 'notice', 'dataDeAbertura', 'process', 'modality', 'object_name', 'price', 'winner', 'created', 'author')

    fieldsets = [
        ('Informações Principais', {'fields': ['year', 'notice', 'situation', 'date', 'process', 'modality', 'object_name', 'price', 'winner', 'document']}),
        ('Informações de Publicação', {'fields': ['status', 'author', 'publish']})
    ]


class YearAdmin(admin.ModelAdmin):
    
    list_display = ('year', 'created', 'author')
    fieldsets = [
        ('Ano', {'fields': ['year']}),
        ('Informações de Publicação', {'fields': ['status', 'author', 'publish']})
    ]

# Register your models here.
admin.site.register(Bidding, BiddingAdmin)
admin.site.register(Year, YearAdmin)