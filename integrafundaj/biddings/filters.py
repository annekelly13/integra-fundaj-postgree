from .models import Bidding
import django_filters

MODALITY_CHOICES = (
    ('electronicTrading', 'Pregão eletrônico'),
    ('electronicTradingPrice', 'Pregão eletrônico por registro de preço'),
    ('competition', 'Concorrência'),
    ('faceTrading', 'Pregão presencial'),
    ('invitation', 'Convite'),
    ('sale', 'Leilão'),
    ('pricingTaking', 'Tomada de preços'),
)

STATUS_CHOICES = (
    ('OpeningSession', 'Sessão de Abertura'),
    ('legalRecourse', 'Recurso'),
    ('award', 'Adjudicação'),
    ('homologation', 'Homologação'),
    ('nullified', 'Anulado'),
    ('revoked', 'Revogado'),
    ('failed', 'Fracassado'),
    ('waste', 'Deserto'),
    ('acceptance', 'Aceitação'),
)


class BiddingFilter(django_filters.FilterSet):
    status = django_filters.ChoiceFilter(choices=STATUS_CHOICES)
    process = django_filters.CharFilter(lookup_expr='icontains')  #
    modality = django_filters.ChoiceFilter(choices=MODALITY_CHOICES)

    class Meta:
        model = Bidding
        fields = ['status', 'process', 'modality']
