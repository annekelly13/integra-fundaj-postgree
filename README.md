# Integra Fundaj
Sistema de Gerenciamento

# Ambiente de Desenvolvimento

1. Clone o repositório.
2. Crie um virtualenv com Python 3.5.0
3. Ative o virtualenv.
4. Instale as dependências
5. Configure a instância com o .env
6. Aplique as migrações e execute o projeto

## Instalação
### Clone o repositório

```console
git clone https://lidymonteiro@bitbucket.org/fundajdeveloper/integra-fundaj.git
cd integra-fundaj
```

### Crie o virtualenv com Python 3.5.0

Unix
```console
python3 -m venv .myvenv
```
Windows
```console
C:\Python35\python -m venv .myvenv
```

### Ative o virtualenv

Unix
```console
source .myvenv/bin/activate
```
Windows
```console
.myvenv\Scripts\activate.bat
```
### Instale as dependencias
```console
pip install -r requirements-dev.txt
```

### Configure a instância com o .env

Unix
```console
cp contrib/env-sample .env
```
Windows
```console
copy contrib\env-sample .env
```

### Execute as migrações
```console
python manage.py migrate --run-syncdb
python manage.py runserver
```